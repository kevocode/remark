<?php

use backend\components\Menu;

?>
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <?php echo Menu::widget([
                    'options'           => [
                        'class'       => 'site-menu',
                        'data-plugin' => 'menu'
                    ],
                    'encodeLabels'      => false,
                    'activeCssClass'    => 'active',
                    'linkTemplate'      => '<a href="{url}" class="{activo}"><i class="site-menu-icon {icon}"></i><span class="site-menu-title">{label}</span>{arrow}</a>',
                    'principalLinkItem' => '<a href="{url}" class="{activo}"><i class="{icon}"></i><span class="site-menu-category">{label}</span></a>',
                    'submenuTemplate'   => '<ul class="site-menu-sub">{items}</ul>',
                    'activateParents'   => true,
                    'itemOptions' => [
                        'class' => 'site-menu-item'
                    ],
                    'items'             => [
                        [ 'label' => 'Menú' ],
                        [
                            'label' => 'Principal',
                            'url'   => [ '/site/index' ],
                            'icon'  => 'fas fa-tachometer-alt'
                        ],
                        [
                            'label' => 'Control de acceso',
                            'url'   => 'javascript:void(0)',
                            'icon'  => 'fa fa-users',
                            'items' => [
                                ['label' => 'Usuarios', 'url' => ['/adm/users/index']],
                                ['label' => 'Roles', 'url' => ['/adm/auth-item/index']],
                                ['label' => 'Permisos', 'url' => ['/adm/auth-item/permission']],
                                ['label' => 'Rutas', 'url' => ['/adm/auth-item/routes']]
                            ]
                        ],
                        [
                            'label' => 'Configuraciones',
                            'url'   => 'javascript:void(0)',
                            'icon'  => 'fa fa-cog',
                            'items' => [
                                [ 'label' => 'Cultivos', 'url' => ['/cultivos/index'] ],
                                [ 'label' => 'Divisiones Políticas', 'url' => ['/divisiones-politica/index'] ],
                                [ 'label' => 'Estados', 'url' => ['/estados/index'] ],
                                [ 'label' => 'Justificaciones de tratamiento', 'url' => ['/justificaciones-tratamiento/index'] ],
                                [ 'label' => 'Niveles de Calificación', 'url' => ['/niveles-calificacion/index'] ],
                                [ 'label' => 'Plagas', 'url' => ['/plagas/index'] ],
                                [ 'label' => 'Países', 'url' => ['/paises/index'] ],
                                [ 'label' => 'Procedencias', 'url' => [ '/procedencias-producto/index' ]],
                                [ 'label' => 'Productos', 'url' => ['/productos/index'] ],
                                [ 'label' => 'Páginas', 'url' => [ '/paginas/index' ] ],
                                [ 'label' => 'Tipos de actividad', 'url' => [ '/tipos-actividad/index' ] ],
                                [ 'label' => 'Tipos de integrante', 'url' => ['/tipos-integrante/index'] ],
                                [ 'label' => 'Tipos de Máquina', 'url' => ['/tipos-maquina/index'] ],
                                [ 'label' => 'Tipos de personas', 'url' => ['/tipos-persona/index'] ],
                                [ 'label' => 'Tipos de Pertenencia', 'url' => ['/tipos-pertenencia/index'] ],
                                [ 'label' => 'Tipos de producto', 'url' => ['/tipos-producto/index'] ],
                                [ 'label' => 'Tipos de vinculación', 'url' => [ '/tipos-vinculacion/index' ] ],
                                [ 'label' => 'Tipos de área', 'url' => ['/tipos-area/index'] ],
                                [ 'label' => 'Unidades', 'url' => ['/unidades/index'] ],
                                [ 'label' => 'Upas', 'url' => [ '/upas/index' ]]
                            ],
                        ]
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>
