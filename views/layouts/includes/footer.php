<style>
    .site-footer {
        height: auto;
    }
    .site-footer-legal {
        margin-top: 10px;
    }
</style>
<footer class="site-footer">
    <div class="site-footer-legal">Copyright © 2018 <strong>TIC Makers</strong></div>
    <div class="site-footer-right">
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-mintic.png" style="height: 40px; margin-left: 10px;" />
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-renata.png" style="height: 40px; margin-left: 10px;" />
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-ticmakers.png" style="height: 40px; margin-left: 10px;" />
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-ginnova.png" style="height: 40px; margin-left: 10px;" />
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-promango.png" style="height: 40px; margin-left: 10px;" />
        <img src="<?php echo $urlBaseTema ?>/assets/images/logo-planntic.png" style="height: 40px; margin-left: 10px;" />
    </div>
</footer>