<?php

use backend\themes\remark\assets\ThemeAsset;
use ticmakers\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

ThemeAsset::register($this);
$urlBaseTema = Yii::$app->assetManager->getPublishedUrl('@app/themes/remark/base');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Yii::$app->html::csrfMetaTags() ?>
    <title><?= Yii::$app->html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="<?= $urlBaseTema ?>/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
</html>
<body class="animsition page-error layout-full">
    <?php $this->beginBody() ?>
    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content vertical-align-middle">
        <?php echo $content ?>
        <?= Yii::$app->html::a(Yii::t('app', 'Ir a la página de inicio'), ['index'], ['class' => 'btn btn-primary btn-round']) ?>
        <footer class="page-copyright">
          <p>TicMakers S.A.S</p>
          <p>© <?= date('Y') ?>. <?= Yii::t('app', 'Todos los derechos reservados') ?>.</p>
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-twitter" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-facebook" aria-hidden="true"></i>
        </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
          <i class="icon bd-dribbble" aria-hidden="true"></i>
        </a>
          </div>
        </footer>
        </div>
    </div>
    <?php $this->endBody() ?>
    <script>
        (function(document, window, $){
            var Site = window.Site
            $(document).ready(function(){
                Site.run();
            })
        })(document, window, jQuery)
    </script>
</body>
<?php $this->endPage() ?>