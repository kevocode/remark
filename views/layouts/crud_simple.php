<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use backend\assets\ThemeAsset;
use backend\components\Menu;
use backend\widgets\ActiveForm;

ThemeAsset::register($this);
$urlBaseTema = Yii::$app->assetManager->getPublishedUrl('@app/themes/remark/base');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="<?= $urlBaseTema ?>/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
    </script>
</head>
</html>
<body class="animsition dashboard">
    <?php $this->beginBody() ?>
    <!-- Navbar top -->
    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided" data-toggle="menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-bar"></span>
            </button>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
                <i class="icon wb-more-horizontal" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
                <img class="navbar-brand-logo" src="<?php echo $urlBaseTema ?>/assets/images/logo-planntic-menu.png" title="<?php echo Yii::$app->name ?>">
                <span class="navbar-brand-text hidden-xs-down"> <?php echo strtoupper(Yii::$app->name) ?></span>
            </div>
        </div>
        <div class="navbar-container container-fluid">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-float" id="toggleMenubar">
                        <a class="nav-link" data-toggle="menubar" href="#" role="button">
                            <i class="icon hamburger hamburger-arrow-left">
                                <span class="sr-only">Toggle menubar</span>
                                <span class="hamburger-bar"></span>
                            </i>
                        </a>
                    </li>
                    <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                        <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                            <span class="sr-only">Toggle fullscreen</span>
                        </a>
                    </li>
                </ul>
                <!-- End Navbar Toolbar -->
                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
                            <span class="avatar avatar-online">
                                <img src="<?php echo $urlBaseTema ?>/global/portraits/5.jpg" alt="...">
                                <i></i>
                            </span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Perfil</a>
                            <div class="dropdown-divider" role="presentation"></div>
                            <a class="dropdown-item" role="menuitem" onclick="$('#frm-logout').submit(); return false"><i class="icon wb-power" aria-hidden="true"></i> Salir</a>
                            <?php $form = ActiveForm::begin([
                                'id' => 'frm-logout',
                                'action' => ['/site/logout'],
                            ]); ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->
        </div>
    </nav>
    <!-- End Navbar top -->
    <!-- Main menu -->
    <?php echo Yii::$app->view->renderFile('@app/themes/remark/layouts/includes/menu.php') ?>
    <!-- End Menu -->
    <!-- Page -->
    <div class="page">
        <div class="page-header">
            <h1 class="page-title"><?php echo $this->title ?></h1>
            <div class="page-header-actions">
                <?php echo Breadcrumbs::widget([
                    'tag'                => 'ol',
                    'options'            => ['class' => 'breadcrumb'],
                    'itemTemplate'       => "<li class=\"breadcrumb-item\">{link}</li>",
                    'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                    'links'              => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
        </div>
        <div class="page-content container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <?php echo Yii::$app->view->renderFile('@app/themes/remark/layouts/includes/footer.php', [
      'urlBaseTema' => $urlBaseTema
    ]) ?>
    <?php $this->endBody() ?>
    <script>
        (function(document, window, $){
            var Site = window.Site
            $(document).ready(function(){
                Site.run();
            })
        })(document, window, jQuery)
    </script>
</body>
<?php $this->endPage() ?>
