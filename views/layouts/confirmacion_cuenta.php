<?php
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
    <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="center">
                                            <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:162px;"><a href="#" target="_blank" ><img alt="" title="" height="auto" src="" style="border:none;border-radius:0px;display:block;outline:none;text-decoration:none;width:100%;height:auto;"
                                                            width="162"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin:0px auto;max-width:600px;background:#00a39e;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#00a39e;" align="center" border="0">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="justify">
                                        <div class="" style="cursor:auto;color:#FFF;font-family:helvetica;font-size:22px;font-weight:bold;line-height:22px;text-align:justify;">Validación de cuenta</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin:0px auto;max-width:600px;background:#FAFAFA;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;background:#FAFAFA;" align="center" border="0">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                        <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="justify">
                                            <div class="" style="cursor:auto;color:#000;font-family:helvetica;font-size:15px;line-height:22px;text-align:justify;">
                                                Usted se ha registrado en la plataforma <strong><?= Yii::$app->name ?></strong>, para activar su cuenta haga click en <strong>Confirmar</strong>.
                                                <br><br>
                                                <a href="<?= Url::home('http') ?>" style="cursor:pointer;width:300px;height:35px;background-color:#00a39e;text-decoration:none;padding:10px 20px;color:white;border-radius:3px;">Confirmar</a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin:0px auto;max-width:600px;">
        <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%;" align="center" border="0">
            <tbody>
                <tr>
                    <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;">
                        <div class="mj-column-per-75 outlook-group-fix" style="vertical-align:middle;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" style="vertical-align:middle;" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="justify">
                                            <div class="" style="cursor:auto;color:#666666;font-family:helvetica;font-size:12px;line-height:22px;text-align:justify;"><?= Yii::$app->name ?> es una solución de TIC Makers S.A.S.<br> Ibágue-Colombia<br> Usted a recibido este correo porque se encuentra registrado en la plataforma <strong>Planntic</strong>.</div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="mj-column-per-25 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;">
                            <table role="presentation" cellpadding="0" cellspacing="0" style="vertical-align:top;" width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td style="word-wrap:break-word;font-size:0px;padding:10px 25px;" align="right">
                                            <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="right" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="width:100px;"><a href="http://www.ticmakers.com/web/es/" target="_blank" ><img alt="" title="" height="auto" src="' . $url . '" style="border:none;border-radius:0px;display:block;outline:none;text-decoration:none;width:100%;height:auto;"
                                                            width="100"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>