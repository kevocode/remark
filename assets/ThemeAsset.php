<?php

namespace backend\themes\remark\assets;

use yii\web\AssetBundle;
/**
 * Assets del tema
 * 
 * @package backend\themes\remark\assets
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 1.0.0
 * @since 0.0.1
 */
class ThemeAsset extends AssetBundle
{

    public $sourcePath = '@backend/themes/remark/base/';
    public $css = [
        'global/css/bootstrap.min.css',
        'global/css/bootstrap-extend.min.css',
        'assets/css/site.min.css',
        'global/vendor/animsition/animsition.css',
        'global/vendor/asscrollable/asScrollable.css',
        'global/vendor/switchery/switchery.css',
        'global/vendor/intro-js/introjs.css',
        'global/vendor/slidepanel/slidePanel.css',
        'global/vendor/flag-icon-css/flag-icon.css',
        'global/fonts/web-icons/web-icons.min.css',
        'global/fonts/brand-icons/brand-icons.min.css',
        // 'global/fonts/font-awesome/font-awesome.min.css',
        'assets/skins/green.min.css',
        'global/vendor/toastr/toastr.min.css',
        'global/vendor/webui-popover/webui-popover.min.css',
        'global/vendor/toastr/toastr.css',
        'assets/examples/css/advanced/toastr.css'
    ];
    public $js = [
        // Core
        'global/vendor/babel-external-helpers/babel-external-helpers.js',
        'global/vendor/animsition/animsition.js',
        'global/vendor/mousewheel/jquery.mousewheel.js',
        'global/vendor/asscrollbar/jquery-asScrollbar.js',
        'global/vendor/asscrollable/jquery-asScrollable.js',
        'global/vendor/ashoverscroll/jquery-asHoverScroll.js',
        // Plugins
        'global/vendor/switchery/switchery.js',
        'global/vendor/intro-js/intro.js',
        'global/vendor/screenfull/screenfull.js',
        'global/vendor/slidepanel/jquery-slidePanel.js',
        'global/vendor/toastr/toastr.min.js',
        'global/vendor/webui-popover/jquery.webui-popover.min.js',
        // Scripts
        'global/js/Component.js',
        'global/js/Plugin.js',
        'global/js/Base.js',
        'global/js/Config.js',
        'assets/js/Section/Menubar.js',
        'assets/js/Section/GridMenu.js',
        'assets/js/Section/Sidebar.js',
        'assets/js/Section/PageAside.js',
        'assets/js/Plugin/menu.js',
        'global/js/config/colors.js',
        'assets/js/config/tour.js',
        'assets/js/Site.js',
        'global/js/Plugin/asscrollable.js',
        'global/js/Plugin/slidepanel.js',
        'global/js/Plugin/switchery.js',
        'global/js/Plugin/responsive-tabs.js',
        'global/js/Plugin/closeable-tabs.js',
        'global/js/Plugin/tabs.js',
        'assets/js/all.min.js',
    ];
    public $depends = [
        \yii\web\YiiAsset::class,
        \yii\bootstrap4\BootstrapAsset::class,
        \yii\bootstrap4\BootstrapPluginAsset::class
    ];

}
